//Note: don't add a semicolon at the end of then().
//Fetch answers must be inside the await () for each function.
//Each function will be used to check the fetch answers.
//Don't console log the result/json, return it.

// Get Single To Do [Sample]
async function getSingleToDo(){

    return await (

       //add fetch here.
       
       fetch('https://jsonplaceholder.typicode.com/todos/2')
       .then((response) => response.json())
       .then((json) => json)
   
   
   );

}



// Getting all to do list item
async function getAllToDo(){

   return await (

      //add fetch here.
fetch('https://jsonplaceholder.typicode.com/todos', {
   method: 'GET',
   headers: {
      'Content-Type' : 'application/json'
   }
})
.then((response) => response.json())
.then((todos) => {
   const titles = todos.map(item => item.title)
   return titles;
})

  );

}

// [Section] Getting a specific to do list item
async function getSpecificToDo(){
   
   return await (

       //Add fetch here.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
   method: 'GET',
   headers: {
      'Content-Type' : 'application/json'
   }
})
.then((response) => response.json())
.then((json) => json)
.then ((json) => `The item '${json.title}' on the list has a status of ${json.completed}`)

   );

}

// [Section] Creating a to do list item using POST method
async function createToDo(){
   
   return await (

       //Add fetch here.
fetch('https://jsonplaceholder.typicode.com/todos', {
   method : 'POST',
   headers : {
      'Content-Type' : 'application/json'
   },
   body: JSON.stringify({
      userId: 1,
      title: "Creat a to do list item",
      completed: true
   })
})
   .then((response) => response.json())
   .then((json) => json)
   );

}

// [Section] Updating a to do list item using PUT method
async function updateToDo(){
   
   return await (

       //Add fetch here.
       fetch('https://jsonplaceholder.typicode.com/todos/1', {
          method : 'PUT',
          headers : {
             'Content-Type' : 'application/json'
          },
          body: JSON.stringify({
             
             title: "Update a to do list item",
             description: "To update to-do list items and more",
             status : "completed",
             dateCompleted : "7/11/23",
             userId: 1
          })
       })
          .then((response) => response.json())
          .then((json) => json)
   );

}

// [Section] Deleting a to do list item
async function deleteToDo(){
   
   return await (

       //Add fetch here.
fetch('https://jsonplaceholder.typicode.com/todos/1', {
          method : 'DELETE'})
       
   );

}




//Do not modify
//For exporting to test.js
try{
   module.exports = {
       getSingleToDo: typeof getSingleToDo !== 'undefined' ? getSingleToDo : null,
       getAllToDo: typeof getAllToDo !== 'undefined' ? getAllToDo : null,
       getSpecificToDo: typeof getSpecificToDo !== 'undefined' ? getSpecificToDo : null,
       createToDo: typeof createToDo !== 'undefined' ? createToDo : null,
       updateToDo: typeof updateToDo !== 'undefined' ? updateToDo : null,
       deleteToDo: typeof deleteToDo !== 'undefined' ? deleteToDo : null,
   }
} catch(err){

}

