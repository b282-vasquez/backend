/*
Introduction to Postman and REST
 ~ Learn how to test an API

/////////////////////////////
 PATCH - Same purpose of PUT 
 PATCH - is used to update a single/several properties 	
 PUT - is used to update the whole object
///////////////////////////////

What is an API? 
(Application Programming Interface)
	- Part of a server responsible for receiving requests and sending responses.

What is it for? 
	- Hide the server beneath an interface layer
		- Hidden complexity makes apps easier to use.
		- Sets the rules of ineraction between front and back ends of an application, improving security.


What is REST?
(REpresentational State Transfer)
	- Architectural style for providing standards for communication between computer system.

What problem does it solve?
	- The need to separate user interface concerns of the client from data storage concerns of the server.

How does it sovle the problem?
	- Statelessness.
	- Standardized communication.

Statelesness
	- Server does not need to know client state and vice versa.
	- Every client request has all the information it needs to be processed by the API.
	- Made possible via resources and HTTP methods.

Standardized Communication
	- Enables a decoupled server to understand, process, and respond to client requests WITHOUT knowing client state.
	- Implemented via resources represented as Uniform Resource Identifier (URI) endpoints and HTTP methods.
		- Resources are plural by convention
			ex:
				- /api/photos(NEVER/api/photo)
				- /api/statuses

	Anatimy of a Client Request
		- Operation to be performed dictated by HTTP methods
			- GET - Recieve info about an API resource
			- POST - Creat
			- PUT - Update
			- DELETE - Delete

		-  HTTP methods and corresponding CRUD operation
			- POST = INSERT
			- PUT = UPDATE
			- GET = SELECT
			- DELETE = DELETE

		- A path to the resource to be operated on.
			- URI endpoint
			http://localhost:4000/api/tasks/5gs1039212

		- A header containing additional request information
			Key: Content-Type 
			Value: JSON Value

		- A request body containing data (optional)
			- raw > JSON
		- Yes, hindi naman lahat ng request recquired na magkaroon ng BODY.

	In the URI
		https://official-joke-api.appspot.com/jokes/programming/random

		- The resource is JOKES
		- /programming denotes the resource category
		- /random determines the specifice joke thatwill be sent  via a response
		 - Hitting enter on youe broiwser send a GET request to the API endpoint
		 - Given a URI ednpoint and a methodm the APU was able to respond.
*/

//////////////////////////////// CODE ALONG SECTION//////////////////////////////////////////////////

// [SECTION] JavaScript Synchronous and Asynchronous
// JS is by default is synchronous, meaning that only one statement us executed at a time
console.log("Hello World!");
console.log("Hello World Again!");

// Aysnchronous means that we can proceed to execute other statements, while consuming is running in the background

// [SECTION] Getting all posts
// The Fetch API allows you to asynchronously request for a resource (data)
// "Promise" is an object that represents the eventual completion or failure of an asynchronous function and its resulting value
/*
SYNTAX:
	fetch('URL')
*/
console.log( fetch('https://jsonplaceholder.typicode.com/posts/') );


// Fetch and .then
/*
SYNTAX:
	fetch('URL')
	.then(response)=> {})
*/


fetch('https://jsonplaceholder.typicode.com/posts/') //"fetch" method will return a "promise" that resolves to a "response object"

.then(response => console.log(response.status)); // ".then" method captures the "response object" and return another promise which will be eventually be resolved or rejected.


fetch('https://jsonplaceholder.typicode.com/posts/')
.then((response) => response.json()) // ".json" method from the response object to convert the data retrieved into JSON format to be used in our application.
.then((json) => console.log(json));// Print the converted JSON value from "fetch request".


/*Async and Await
	- The "async" and "await" keywords is another approach that can be used to achieve asynchronous code
	- Used in functions to indicate which portions of code should be waited for.*/
/*

async function fetchData() {
	let result = await fetch('https://jsonplaceholder.typicode.com/posts/'); //waits for the "fetch" method to complete then stores the value in the "result" variable.
		console.log(result); // Result returned by fetch is a returns a promise.
		console.log(typeof result); // The returned "Response" is an object.
		console.log(result.body); // We cannot access the content of the "Response" by directly accessing it's body property.

	let json = await result.json() // Converts the data from the "Response" object as JSON
		console.log(json); 	// Print out the content of the "Response" object
};
fetchData();

*/

// [SECTION] Creating a post
/*
SYNTAX:
	fetch('URL', options)
	.then((response)=>{})
	.then((response)=>{})
*/

fetch("https://jsonplaceholder.typicode.com/posts/", {
	// Sets the method of the "Request" object to "POST" following REST API
	// Default method is GET
	method: 'POST',
	// Sets the header data of the "Request" object to be sent to the backend
	// Specified that the content will be in a JSON structure
	headers: {
		'Content-Type': 'application/json'
	},
	// Sets the content/body data of the "Request" object to be sent to the backend
	// JSON.stringify converts the object data into a stringified JSON
	body: JSON.stringify({
		title: 'New Post', 
		body: "Hello world!",
		userId: 1
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Updating a post

// PUT - method used to update the whole document object
// PATCH - method used to update a single/serveral properties

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: "Updated post",
		body: "Hello again!"
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


// [SECTION] Delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
});



