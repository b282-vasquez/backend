const Course = require("../models/Course");

/*
Controllers for creating a new course
Business Logic:
1. Create a new Course object using the mongoose model and the information from the request body and the id from the header
2. Save the new Course to the database
*/

// S39 Activity Start
module.exports.addCourse = (data) => {
	// User is an admin
	if (data.isAdmin) {
		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});
		// Saves the created object to our database
		return newCourse.save().then((course, error) => {
			// Course creation successful
			if (error) {
				return false;
			// Course creation failed
			} else {
				return true;
			};
		});

	};
	// Since Promise.resolve() returns a resolved promse, the variable message will already be in a resolved status.
	let message = Promise.resolve("User must be Admin to access this.")
	return message.then((value) => {
		return {value}
	});
};
// S39 Activity End


// module.exports.addCourse = (reqBody) => {
// 	let newCourse = new Course({
// 		name: reqBody.name,
// 		description: reqBody.description,
// 		price: reqBody.price
// 	});

// 	return newCourse.save().then((course, error) => {
// 		if(error) {
// 			return false;
// 		} else {
// 			return true;
// 		};
// 	});
// };

// Controllers for retrieving ALL the courses

module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	});
};

// Controllers for retrieving ACTIVE COURSE
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result;
	});
};


// Controllers for retrieving specific course
module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	});
};

// Controllers for updating a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};

// Controllers for archieving a course


module.exports.archiveCourse = (reqParams) =>{
	let updateActiveField = {
		isActive: false
	};

	return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, error) => {
		if (error) {
			return false;
		} else {
			return true;
		};
	});
};
 

/*module.exports.archiveCourse = (reqParams, data) => {
	if (data.isAdmin) {
		let updateActiveField = {
			isActive: false
		};
		return Course.findByIdAndUpdate(reqParams.courseId, updateActiveField).then((course, err) => {
			if(err){
				return false;
			} else {
				return true;
			};
		});
	};
	let message = Promise.resolve("User must be Admin to accrss this.")
	return message.then((value) => {
		return{value}
	});
};*/