const express = require("express");
const mongoose = require("mongoose");
/* 
require("cors")
- Allows our backend application to be AVAILABLE to our frontend application
- Allows us to CONTROL the (app's) "Cross Origin Resource Sharing" settings*/
const cors = require("cors");

// Allows ACCESS to ROUTES defined within our application
const userRoute = require("./routes/userRoute");
const courseRoute = require("./routes/courseRoute");

// Creates an ("app" variable) that stores the result of the ("express" function) that INITIALIZES our (express application) and ALLOWS us ACCESS to DIFFERENT METHODS that will make backend creation easy
const app = express();

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://mrvasquezr:o1FY87gxoTRobrX5@wdc028-course-booking.edafbzc.mongodb.net/courseBookingAPI",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));
/* app.use(cors())
 	- Allows ALL RESOURCES TO ACCESS our backend application*/
app.use(cors());

// Defines the ("/users" string) TO BE INCLUDED for ALL USERS ROUTES defined in the "user" route file
//example: http://localhost:4000/users
app.use("/users", userRoute);

// Defines the ("/courses" string) TO BE INCLUDED for ALL COURSES ROUTES defined in the "course" route file
//example: http://localhost:4000/courses
app.use("/courses", courseRoute);

// ("process.env.PORT") is an environment variable that typically holds the port number on which the server should listen.
app.listen(process.env.PORT || 4000, () => console.log(`Now listening to port ${process.env.PORT || 4000}!`));