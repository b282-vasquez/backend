// Course.js - the first letter should be capitalized and singular in form

const mongoose = require("mongoose");

const courseSchema = new mongoose.Schema({
	name: {
		type: String,
		// Requires the data for our fields/properties to be included when creating a document/record
		required: [true, "Course name is required!"]
	},
	description: {
		type: String,
		required: [true, "Description is required!"]
	},
	price: {
		type: Number,
		required: [true, "Price is required!"]
	},
	isActive: {
		type: Boolean,
		default: true
	},
	createdOn: {
		type: Date,
		// The "new Date()" expression instantiates a new "data" that stores the current date and time whenever a course is created in our database
		default: new Date()
	},
	enrollees: [
		{
			userId: {
				type: String,
				required: [true, "UserId is required"]
			},
			enrolledOn: {
				type: Date,
				default: new Date()
			}
		}
	]
});


module.exports = mongoose.model("Course", courseSchema);