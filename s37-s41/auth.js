/* JSON Web Token or JWT 
		- is a WAY of SECURELY PASSING INFORMATION from the SERVER to the frontend or to other parts of server.*/
const jwt = require("jsonwebtoken");

/* "secret"
		- User defined string data that will be used to create our JSON web tokens
		- Used in the algorithm for encrypting our data which makes it difficult to decode the information without the (defined secret keyword.)*/
const secret = "CourseBookingAPI";


//==================== Token CREATION
module.exports.createAccessToken = (user) => {
	// The "data" will be received from the registration form
	// When the user logs in, a token will be created with user's information
	// payload
	const data = {
		id : user._id,
		email : user.email,
		isAdmin : user.isAdmin
	};

	// Generate a JSON web token using the (jwt's sign method)
	// Generates the token using the (form data) and the (secret code) with (no additional options) provided
	return jwt.sign(data, secret, {});
};


// ===================== Token VERIFICATION
module.exports.verify = (req, res, next) => {

	// The token is RETRIEVED from the request header (req.header)
	/*This can be provided in postman under_Authorization -> Bearer Token*/
	let token = req.headers.authorization;

	// Token RECIEVED and is NOT undefined
	if(typeof token !== "undefined") {
		/* console.log(token); 
				result: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YTJhM2JlZjhjMzgxN2YwN2ExYWNlMCIsImVtYWlsIjoiamFuZUBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2ODgzODA5NDB9.NF3hZT-EZtU5frY3n9p05fwvtvzK_YnC1AohEflElv0
*/
		/*
		- The ("slice" method) takes only the token from the information sent via the request header
		- The token sent is a type of ("Bearer" token) which when received contains the word "Bearer " as a prefix to the string
		- This removes the "Bearer " prefix and obtains only the token for verification
		RESULT: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjY0YTJhM2JlZjhjMzgxN2YwN2ExYWNlMCIsImVtYWlsIjoiamFuZUBtYWlsLmNvbSIsImlzQWRtaW4iOmZhbHNlLCJpYXQiOjE2ODgzODA5NDB9.NF3hZT-EZtU5frY3n9p05fwvtvzK_YnC1AohEflElv0 */
		token = token.slice(7, token.length);
		
		// Validate the token using the ("verify" method) decrypting the token using the secret code
		return jwt.verify(token, secret, (err, data) => {
			
			if(err) { // If JWT is NOT VALID
				return res.send({auth: "failed"});
			
			} else { // If JWT is VALID
				
				next(); // Allows the application to PROCEED with the NEXT middleware function/callback function in the route
			}
		})
	
	} else { // Token does not exist
		return res.send({auth: "failed"});
	};
};


//================== Token DECRYPTION
module.exports.decode = (token) => {
	// Token received and is not undefined
	if (typeof token !== "undefined") {
		// Retrieves only the token and removes the "Bearer " prefix
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data) => {
			if (err) {
				return null;
			} else {
				// The "decode" method is used to obtain the information from the JWT
				// The "{complete:true}" option allows us to return additional information from the JWT token
				// Returns an object with access to the "payload" property which contains user information stored when the token was generated
				// The payload contains information provided in the "createAccessToken" method defined above (e.g. id, email and isAdmin)
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	// Token does not exist
	} else {
		return null;
	};
};

/*
module.exports.isUserAdmin = (req, res, next) => {
	let token = req.headers.authorization;

	if(typeof token !== "undefined"){
		token = token.slice(7, token.lenght);
			return jwt.verify(token, secret, (err, data) => {
			if (err){
				return null
			} else {
				return jwt.decode(token, {complete:true}).payload;
			}
		})
	} else {
		return null;
	};

};*/