const express = require("express");

const router = express.Router();

const courseController = require("../controllers/courseController");

const auth = require("../auth");


/*
Route for creating course

router.post("/create", (req, res) => {
	courseController.addCourse(req.body).then(resultFromController => res.send(resultFromController));
});
*/
// S39 Activity Start
router.post("/create", auth.verify, (req, res) => {

	const data = {
		course: req.body,
		//course & isAdmin - name lang na pwedeng baguhin
		isAdmin: auth.decode(req.headers.authorization).isAdmin // .isAdmin para makuha kung admin ba siya. 
	}

	courseController.addCourse(data).then(resultFromController => res.send(resultFromController));
});
// S39 Activity End


// Route for retrieving all the courses

router.get("/all", (req, res) => {
	courseController.getAllCourses().then(resultFromController => res.send(resultFromController));
});


// Route for retrieving active courses

router.get("/active", (req, res) => {
	courseController.getAllActive().then(resultFromController => res.send(resultFromController));
});

// Route for retrieve specific course
//Using Dynamic route
router.get("/:courseId", (req, res) => {
	courseController.getCourse(req.params).then(resultFromController => res.send(resultFromController));
});

// Route for updating a course
// with verification "auth.verify"
router.put("/:courseId", auth.verify, (req, res) => {
	courseController.updateCourse(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route for Archiving a course

router.patch("/:courseId/archive", auth.verify, (req, res) => {
	courseController.archiveCourse(req.params).then(resultFromController => res.send(resultFromController));
});


/* "Using IsAdmin"
router.patch("/:courseId/archive", auth.verify, (req, res) => {
	const data = {
		course: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	courseController.archiveCourse(req.params, data).then(resultFromController => res.send(resultFromController));
});
*/






module.exports = router;