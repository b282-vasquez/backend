// Routes CONTAINS ALL the ENDPOINTS for our application

const express = require("express");

const router = express.Router();

const userController = require("../controllers/userController");

const auth = require("../auth");


// Routes for checking if the user's email already exist in the database
router.post("/checkEmail", (req, res) => {
	userController.checkEmailExists(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for user registration
router.post("/register", (req, res) => {
	userController.registerUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for user AUTHENTICATION (login)
router.post("/login", (req, res) => {
	userController.loginUser(req.body).then(resultFromController => res.send(resultFromController));
});


// Route for retrieving user details
// The ("auth.verity") ACTS as a MIDDLEWARE to ensure that the user is logged in before they can enroll to a course
router.get("/details", auth.verify, (req, res) => {

	// Uses the ("decode" method) defined in the ("auth.js" file) to RETRIEVE the USER INFORMATION from the token passing the "token" from the request header as an argument. (req.headers.authorization)
	const userData = auth.decode(req.headers.authorization)

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId: userData.id}).then(resultFromController => res.send(resultFromController));
});

// Route to enroll user to a course

router.post("/enroll", auth.verify, (req, res) => {
		
		let data = {
			userId: auth.decode(req.headers.authorization).id,
			courseId: req.body.courseId
		}
		userController.enroll(data).then(resultFromController => res.send(resultFromController));
});




module.exports = router;