// console.log("I will be a web developer!");

// Array Methods
/*
 - JS has buit-in functions and methods for arrays
 - This allows us to manipulate and access array items
*/

// [SECTION] Mutators Methods
/*
 - Mutator methods are functions that mutate or change an array after they are created.
*/

let fruits = ['Apple', 'Orange', 'Kiwi', 'Dragon fruit'];
console.log('Original Array: ');
console.log(fruits);

// push() Method
/*
 - Adds an element in the END of an array and returns the updated array's length

SYNTAX:
	arrayName.push();

*/

let fruitsLength = fruits.push('Mango');
console.log('fruitsLength: ');
console.log(fruitsLength); // 5
console.log('Mutated array from push() method: ');
console.log(fruits); //['Apple', 'Orange', 'Kiwi', 'Dragon fruit', 'Mango']

// Pushing multiple elements to an array

fruitsLength = fruits.push('Avocado','Guava');
console.log('fruitsLength: ');
console.log(fruitsLength); // 7 
console.log('Mutated array from push() method: ');
console.log(fruits); // ['Apple', 'Orange', 'Kiwi', 'Dragon fruit', 'Mango', 'Avocado', 'Guava']


// pop()
/*
	- Removes the last element and return the removed element

SYNTAX:
	arrayName.push();
*/

console.log("Current Array: ")
console.log(fruits)

let removedFruit = fruits.pop();

console.log("removedFruit: ")
console.log(removedFruit); // Guava
console.log('Mutated array from pop() method: ');
console.log(fruits); // ['Apple', 'Orange', 'Kiwi', 'Dragon fruit', 'Mango', 'Avocado']

// unshift() method
/*
	- it adds one or more elements at the BEGINNING of an array and it returns the updated array's length

	SYNTAX:
		arrayName.unshift('elementA');
		arrayName.unshift('elementA', 'elementB'..);
*/
console.log('Current Array: ');
console.log(fruits); //['Apple', 'Orange', 'Kiwi', 'Dragon fruit', 'Mango', 'Avocado']

fruitsLength = fruits.unshift('Lime','Banana');

console.log('fruitsLength: ');
console.log(fruitsLength);
console.log('Mutated array from unshift() method: ');
console.log(fruits); //['Lime', 'Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon fruit', 'Mango', 'Avocado']


// shift() method
/*
	- Removes an element at the beginning of an array and returns the removed element

	SYNTAX: 
		arrayName.shift();
*/
console.log('Current Array: ');
console.log(fruits); //['Lime', 'Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon fruit', 'Mango', 'Avocado']

removedFruit = fruits.shift();

console.log("Removed fruit: ")
console.log(removedFruit);
console.log('Mutated array from the shift() method: ');
console.log(fruits);

// splice() method
/*
	- Simultaneously removes an elements from a specified index number and adds element.

	SYNTAX:
		arrayName.splice(startingIndex, deleteCount, elementsToBeAdded)
*/
console.log("Current Array: ") // ['Banana', 'Apple', 'Orange', 'Kiwi', 'Dragon fruit', 'Mango', 'Avocado']
console.log(fruits);

fruits.splice(1, 2, 'Lime', 'Cherry');

console.log('Mutated array from splice method');
console.log(fruits);


// sort() 
/*
	- rearranges the array element in alphanumeric order

	SYNTAX:
		arrayName.sort()
*/

console.log('Current Array: ')
console.log(fruits)

fruits.sort()

console.log('Mutated array from sort() method: ');
console.log(fruits);


// reverse() method
/*
	- reverses the order of array element

	SYNTAX:
		arrayName.reverse()
*/
console.log('Current Array: ')
console.log(fruits)

fruits.reverse();

console.log('Mutated array from reverse() method: ');
console.log(fruits);


// [SECTION] Non-mutator methods 
/*
	- Non-mutator methods are functions that do not modify or change an array after ther are created.
*/

let countries = ['US', 'PH', 'CA', 'SG', 'TH', 'PH', 'FR', 'DE'];

console.log(countries);

// indexof()
/*
	- returns the index number of the FIRST matching element found in an array

NOTE: If no match was found, the result will be "-1"

	SYNTAX:
		arrayName.indexOf(searchValue);
		arrayName.indexOf(searchValue, startingIndex);
*/

let firstIndex = countries.indexOf('PH');
console.log('Result of firstIndex: ');
console.log(firstIndex); // 1

firstIndex = countries.indexOf('PH', 2); // 5
console.log('Result of firstIndex:')
console.log(firstIndex); // 5

// This is no match found.
firstIndex = countries.indexOf('BR');
console.log('Result of firstIndex: ');
console.log(firstIndex); // -1

// lastIndexOf()
/*
	- returns the index number of the LAST matching element found in an array.
	SYNTAX: 
		arrayName.lastIndexOf(searchValue);
		arrayName.lastIndexOf(searchValue, startingIndex);
*/

let lastIndex = countries.lastIndexOf('PH');
console.log('Result of lastIndex: ');
console.log(lastIndex); // 5

// slice() method
/*
	- portions/slices elements from an arrat and return a new array

	SYNTAX:
		arrayName.slice(startingIndex);
		arrayName.slice(startingIndex, endingIndex);
*/

let slicedArrayA = countries.slice(2);
console.log('Result from slice() method: ');
console.log(slicedArrayA);


/*
Example with 'start and ending' Index
Note: 
The element that will be sliced are elements from starting index until the element BEFORE the ending index.
*/
let slicedArrayB = countries.slice(2, 4);
console.log('Result from slice() method: ');
console.log(slicedArrayB); // ['CA', 'SG']


// toString() method
/*
	- return an array as STRING separated by commas

	SYNTAX:
		arrayName.toString();
*/

let stringArray = countries.toString();
console.log('Result from toString() method: ');
console.log(stringArray);
console.log(typeof stringArray);

// [SECTION] Iteration Methods
/*
	- loops designed to perform repetitive task
	- loods over all items in an array
*/

/*
	forEach() method
		- similar to a for loop that iterates on each array of element

	SYNTAX:
		arrayName.forEach(function(indinElement) {
				statement
		})
*/


countries.forEach(function(country) {
	console.log(country);
});


// map() method
/*
	- iterates on each element and returns new array with different values depending in the result of the function's operation.

	SYNTAX:
		let/const resultArray = arrayName.map(function(indivElement){
			statement
		})
*/
let numbers = [1, 2, 3, 4, 5]

let numberMap = numbers.map(function(number){
	return number * number;
})
console.log("Original Array: ")
console.log(numbers);

console.log("Result of map() method: ")
console.log(numberMap);


// filter() method
/*
	- returns ne array that contains elements which meets the given condition.
	SYNTAX:
		let/const resultArray = arrayName.filter(function(indivElement){
			statement
		})
*/

let filterValid = numbers.filter(function(number){
			return (number < 3);
		})
console.log("Result of filter method:");
console.log(filterValid);

// includes() method
/*
	- checks if the argument passed can be found in the array
	- answerable by TRUE or FALSE
	SYNTAX:
		arrayName.includes(<argumentTofind>)
*/

let product = ['Mouse', 'Keyboard', 'Laptop', 'Monitor'];

let productFound1 = product.includes("Mouse");
console.log("Result from includes() method: ")
console.log(productFound1); //True

let productFound2 = product.includes("Headset");
console.log("Result from includes() method: ")
console.log(productFound2); //False

