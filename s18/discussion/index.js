// console.log("Hello World!");

/*Functions (basic)*/

/*EXAMPLE of FUNCTION with PROMPT 
function printInput() {
	let nickname = prompt("Enter your nickname:");
	console.log("Hi, " + nickname);
}
printInput();

END OF EXAMPLE*/ 

/*
FUNCTION with PARAMETERS and ARGUMENT

PARAMETER 
	- acts as a named variable/cotainer that exist only inside a function.
ARGUMENT
	- the information/data provided DIRECTLY into the function.
*/
/* It can assigned a variable
	let name = "Patricia"
*/
function printName (name) //<---This is a PARAMETER 
{
	console.log("My name is " + name);
}
printName("Paeng"); //<--- This is an ARGUMENT 

/*NOTE: - It's REUSEABLE.*/
printName("Ira");
printName("PaengIra");

// Variables can also be passed as an argument.
let sampleVariable = "Ira";
printName(sampleVariable);



function checkDivisibilityBy8(num) {
	let remainder = num % 8;
	console.log("The remainder of " + num + " divided by 8 is: " + remainder);
	let isDivisibleBy8 = remainder === 0;
	console.log("Is " + num + " divisible by 8?")
	console.log(isDivisibleBy8);
}
checkDivisibilityBy8(64);
checkDivisibilityBy8(65);

/*FUNCTION as ARGUMENTS*/

function argumentFunction() {
	console.log("This function was passed as an argument before the message was printed.");
}

function invokeFunction(argumentFunction) {
	argumentFunction();
}
invokeFunction(argumentFunction);
console.log(argumentFunction);

/*Multiple ARGUMENTS 
	- will CORRESPOND to the number of PARAMETERS DECLARED in a function in SUCCEDING order.*/

function createFullName(firstname, middleName, lastName) {
	console.log(firstname + ' ' + middleName + ' ' + lastName);
}
// "Juan" will be stored in the parameter "firstName"
// "Dela" will be stored in the parameter "middletName"
// "Cruz" will be stored in the parameter "lastName"
createFullName('Juan', 'Dela', 'Cruz');
createFullName('Juan', 'Dela', 'Cruz', "Hello");
createFullName('Juan', 'Dela');

// Using variable as ARGUMENTS
let firstname = "John";
let middleName = "Doe";
let lastName = "Smith";

createFullName(firstname, middleName, lastName);

function createFullName(middleName, firstname, lastName) {
	console.log(firstname + ' ' + middleName + ' ' + lastName);
}
// "Dela" will be stored in the parameter "firstName"
// "Juan" will be stored in the parameter "middletName"
// "Cruz" will be stored in the parameter "lastName"
createFullName('Juan', 'Dela', 'Cruz');



