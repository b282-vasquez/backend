// [SECTION] Syntax, Statements, and Comments

// Comments are parts of the code that gets ignored by the language
// Comments are meant to descrive the written code.

/*
There are two types of comments: 
	1. Single-line comment - denotes by two slashes (ctr+/)
	2. Multi-line - denoted by a slash and asterisk	(ctr+shift+/)
*/

/*

Statements
	-  are INSTRUCTIONS that WE tell the computer to perform.

*/
// Example start:
console.log("Hello World!");
// Example end:

/*
Whitespace
	- (basically, spaces and line breaks) can impact functionality in many computer languages but NOT IN JAVASCRIPT.
*/
// Example start: 
console. log ( "Hello World!" ) ;

console.

log	

(
		"Hello World!"
) ;
// Example end


/*
Syntax 
	- it is the SET OF RULES that DESCRIBES how STATEMENTS must be costructed.
	Example start: 
			let myVariables;
	Example end
*/

// [SECTION] Variables

/*
Variables
	- It is used to CONTAIN DATA.

Memory
	- Any information that is USED by an application is STORED.
*/

/*Declaring Variables 
	- Tells our device that a VARIABLE NAME is CREATED and read to store data.

SYNTAX:
	let/const variableName;
*/
// Example Start:
let myVariables;
console.log(myVariables); //undefined coz no given value.
// Example End

/*
WRONG SAMPLE START:
console.log(hello); //Uncaught ReferenceError: Cannot access 'hello' before initialization
let hello;
WRONG SAMPLE END
*/


/*
Naming Convention

Camel Case: 
	- Words are joined together without spaces, and each word, EXCEPT THE FIRST ONE, starts with a CAPITAL LETTER. 
		(For example, myVariableName)

Pascal Case: 
	- Similar to camel case, but the FIRST LETTER of EACH WORD is CAPITALIZED. 
		(For example, MyVariableName)

Kebab Case: 
	- Words are JOINED TOGETHER with hyphens (-) and ALL letters are LOWERCASE. 
		(For example, my-variable-name)

Snake Case: 
	- Words are written in ALL LOWERCASE letters and SEPARATED by underscores (_). 
		(For example, my_variable_name)

*/

// Declaring and Initializing variables

/*
Initializing Variables 
	- the instance when a VARIABLE is GIVEN it's INITIAL value/starting value

SYNTAX: 
	 let/const variableName = value;

*/

// EXAMPLE START:
let productName = "desktop computer"; //string value
console.log(productName); // result desktop computer

let productPrice = 18999; //integer value
console.log(productPrice) //result: 1899

const interest = 3.536; //const - hindi na magbabago
console.log(interest);
// EXAMPLE END

/*
Reassigning Variable Value
	- means CHANGING its INITIAL VALUE or previous value into another value

SYNTAX:
	variableName = newValue;

*/
// EXAMPLE START:
productName = "Laptop";
console.log(productName); // result: Laptop instead of desktop computer.
// EXAMPLE END

/* NOTE :
>>>>The "let variable" CANNOT re-declared WITHIN its SCOPE.

EXAMPLE START:
let friend = "Kate";
let friend = "Jane"; //Uncaught SyntaxError: Identifier 'friend' has already been declared
EXAMPLE END

*/

/* NOTE :
>>>>The "const variable" is CANNOT be re-declared.

EXAMPLE START:
const interest = 3.536;
interest = 77.77;
console.log(interest); //index.js:90 Uncaught TypeError: Assignment to constant variable.
EXAMPLE END
*/

/*Reassigning variables vs. Initializing variables*/

/*EXAMPLE of DECLARATING and INITIALIZING VARIABLES: START*/

// Declaration
let supplier;

// Initialization
supplier = "John Smith Tradings";
console.log(supplier);

/*EXAMPLE of DECLARATING and INITIALIZING VARIABLES: END*/

/*EXAMPLE of REASSIGNMENT VARIABLES: START*/
supplier = "Zuitt Store"
console.log(supplier); // result: Zuitt Store
/*EXAMPLE of REASSIGNMENT VARIABLES: END*/

/*
var 
	- is also in declaring a variable
Hoisting 
	- JavaScript DEFAULT behavior of MOVING INITITALIZATION to top.
*/
// EXAMPLE START:
a = 5;
console.log(a);
var a;
// EXAMPLE END:

/*
EXAMPLE of Hoisting using "let".
a = 5;
console.log(a); //Uncaught ReferenceError: Cannot access 'a' before initialization
let a;
EXAMPLE END.
*/

/*
let/const local/global scope

Scope 
	- essentially means WHERE these VARIABLE are AVAILABLE for USE.
NOTE:
>>>>>let and const are BLOCKED scoped

BLOCK
	- is a CHUNK of CODE bounded by curly braces({}). 
*/
// EXAMPLE START:
let outerVariable = "hello";
{
	let	innerVariable = "hello again";
	console.log(innerVariable);
}
console.log(outerVariable);
// console.log(innerVariable); //index.js:125 Uncaught ReferenceError: innerVariable is not defined
// EXAMPLE END

// Multiple variable declaration
let productCode = 'DC017', productBrand = 'Dell'
console.log(productCode, productBrand)

// // Using a variable with a reserved keyword
// const let = 'hello'
// console.log(let) //Uncaught SyntaxError: let is disallowed as a lexically bound name


// [SECTION] Data types
/*
Strings 
	- are series of characters that create a word, phrase, sentence or anything related to creating text. 
	- be wriiten using either a single (' ') or (" ") qoute
*/
// EXAMPLE START:
let country = 'Philippines';
let province = "Metro Manila";
// EXAMPLE END

/*
Concatenating strings
	- Multiple string values can be combined to create a single string using the "+" symbol
*/
// EXAMPLE START:
let fullAddress = province + ", " + country;
console.log(fullAddress);

let greeting = "I live in the " + country;
console.log(greeting);
// EXAMPLE END

/*
Escape character (\)
	- (\n) refers to creating a new line in betweet text. */
let mailAddress = 'Metro Manila\n\nPhilippines';
console.log(mailAddress);

let message = "John's employees went home early";
console.log(message); //John's employees went home early

message = 'John\'s employees went home early';
console.log(message);

// [NUMBER] Data type
// Integer/Whole Number
let headcount = 26;
console.log(headcount);

// Decimal Number/Fractions
let grade = 98.7;
console.log(grade);

// Exponential Notation 
let planerDistance = 2e10;
console.log(planerDistance);

// Combining number data type and string

console.log("John's grade last quarter is " + grade);

/*[BOOLEAN] Data type
Boolean values 
	- are normally used to store values relating to the state of certain things
*/
let isMarried = false;
let inGoodConduct = true;

console.log("isMarried: " +isMarried);
console.log("inGoodConduct: " +inGoodConduct);

/*
Arrays
	- are SPECIAL KIND of OBJECT DATA TYPES that's USED to store MULTIPLE VALUES.

SYNTAX: 
	let/const arrayName = [elementA, elementB, elementC, ..]
*/

// similar data types
let grades = [98.7, 93.4, 91.3, 97.9];
console.log(grades);

// different data types (NOT RECOMMENDED)
let details = ["John", "Smith", 32, true];
console.log(details);


/*
Objects 
	- is a SPECIAL KIND of DATA TYPES that's USED to MIMIC real world objects/items.

SYNTAX:
	let/const objectName = {
		propertyA: value,
		propertyB: value,
	}
*/

let person = {
		fullName: "Juan Dela Cruz",
		age: 35,
		isMarried: false,
		contact: ["+63932 233 2222", "3342 5342"],
		address: {
			houseNember: "324",
			city: 'Manila'
		}
}
console.log(person);

console.log(typeof grade);
console.log(typeof grades);
console.log(typeof person);
console.log(typeof isMarried);

/*
Null data type
	- It is used to intentionally EXPRESS the ABSENCE of a VALUE in a VARIABLE DECLARATION/INITIALIZATION.
*/
let spouse = null
console.log(spouse);


/*
Undefined
	- Prepresent the STATE of a VARIABLE that has been DECLARED but WITHOUT an ASSIGNED VALUE.
*/
let fullName;
console.log(fullName);