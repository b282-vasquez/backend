// Contain instructions on HOW your API will perform its intended task
// All the operations it can do will be placed in this task

const Task = require("../models/task");

// Controller for getting all the tasks
module.exports.getAllTasks = () => {
	return Task.find({}).then(result => {
		return result;
	});
};

// Controller for creating a task

module.exports.createTask = (requestBody) => {
	let newTask = new Task({
		name: requestBody.name
	})
	return newTask.save().then((task, error) => {
		if(error){
			console.log(error)
			return false;
		} else {
			return task
		};
	})
};

// Controller for deleting a task

module.exports.deleteTask = (taskId) => {
	return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
		if(err) {
			console.log(err);
			return false;
		} else {
			return "Deleted task."
		};
	});
};

// Controller for updating a task

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId).then((result, err) => {
		if(err) {
			console.log(err);
			return false;
		} 
		result.name = newContent.name;

		return result.save().then((updateTask, saveErr) => {
			if(saveErr){
				console.log(saveErr);
				return false;
			} else {
				return "Task updated."
			};
		});
	});
};

// Activity
// Getting one task
module.exports.getOneTask = (taskId) => {
	return Task.findById(taskId).then(result => {
		return result;
	});
};

//Updating Status
module.exports.completeTask = (taskId) => {
    return Task.findById(taskId).then((result, err) => {
        if (err) {
            console.log(err);
            return false;
        } else {
            result.status = "complete";
            return result.save().then((updatedTask, saveErr) => {
                if (saveErr) {
                    console.log(saveErr);
                    return false;
                } else {
                    return  updatedTask;
                };
            });
        };
    });
}
