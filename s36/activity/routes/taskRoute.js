// This folder Defines WHEN particular controllers will used
// Contains all the endpoints for our application

const express = require("express");

// Creates a Router instance that functions as a middleware and routing system
const router = express.Router();
// The "taskController" allows us to use the functions defined in the "taskController.js" file
const taskController = require("../controllers/taskController");


// [SECTION] Routes
// The routes are responsible for defining the URIs that our client accessed and the corresponding controller functions that will be used when a route is accessed


// http://localhost:3001/tasks/
router.get("/", (req,res) => {
	// Invokes the "getAllTasks" function from the "controller.js" file and sends the result back to the client/Postman
	taskController.getAllTasks().then(resultFromController => res.send(resultFromController));
});


// Route to create a new task
router.post("/", (req, res) => {
	taskController.createTask(req.body).then(resultFromController => res.send(resultFromController));
});

// Delete Task

/*
Business Logic
1. Look for the task with the corresponding id provided in the URL/route
2. Delete the task using the Mongoose method "findByIdAndRemove" with the same id provided in the route
*/

// http://localhost:3001/tasks/649ada7bde459492c3c44d6f
router.delete("/:id", (req, res) => {
	taskController.deleteTask(req.params.id).then(
		resultFromController => res.send(
			resultFromController));
});


// Updating a task
router.put("/:id", (req, res) => {
	taskController.updateTask(req.params.id, req.body).then(
		resultFromController => res.send(
			resultFromController));
});

//Activity
//get one task
router.get("/:id", (req,res) => {
	taskController.getOneTask(req.params.id).then(resultFromController => res.send(resultFromController));
})

//update status
router.put("/:id/complete", (req, res) => {
    taskController.completeTask(req.params.id).then(resultFromController => res.send(resultFromController));
});



module.exports = router;