/*====================================================
Express Application
1. npm init --y
2. npm install express mongoose
3. npm install -g nodemon
4. touch .gitignore index.js
5. Create a folders and files
     models → task.js
     routes→ taskRoute.js
     controllers → taskController.js

To run the express application:
nodemon index.js
===================================================*/

const express = require("express");
const mongoose = require("mongoose");


// This allows us to use all the routes defined in "taskRoute.js"
const taskRoute = require("./routes/taskRoute");

const app = express();

const port = 3001;

// Connecting to MongoDB Atlas
mongoose.connect("mongodb+srv://mrvasquezr:o1FY87gxoTRobrX5@wdc028-course-booking.edafbzc.mongodb.net/s36",
	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}
);

// Connecting to MongoDB locally
mongoose.connection.once("open", () => console.log("We're connected to the cloud database!"));

// Middleware
app.use(express.json());
app.use(express.urlencoded({extended: true}));

// Add the task route
// Allows all the task routes created in the "taskRoute.js" file to use "/task" route
// http://localhost:3001/tasks
app.use("/tasks", taskRoute);







app.listen(port, () => console.log(`Server running at port ${port}!`));