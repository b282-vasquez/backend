// [SECTION Arrays]

/*
SYNTAX:
    let/const arrayName = [elementA, elementB, elementC..]
*/

let studentNumbers = ['2020-1923', '2020-1924', '2020-1925', '2020-1926', '2020-1927'];

// Common examples of arrays
let grades = [98.5, 94.3, 89.2, 90.1];
console.log(grades);

let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
console.log(computerBrands);

// Possible use of an array but is NOT recommended
let mixedArr = [12, 'Asus', null, undefined, {}];
console.log(mixedArr);

// Alternative ways to write arrays
let myTasks = [
    'drink html',
    'eat javascript',
    'inhale css',
    'bake sass'
];
console.log(myTasks);

// Creating an array with values from variables

let city1 = "Tokyo";
let city2 = "Manila";
let city3 = "Jakaarta";

let cities = [city1, city2, city3];
console.log(cities);

// [SECTION] length property
console.log("Result of myTasks.lenght");
console.log(myTasks.length);
console.log('Result of cities.length');
console.log(cities.length);

let blackArr = [];
console.log("Result of blankArr.length");
console.log(blackArr.length);

myTasks.length = myTasks.length-1;
console.log("Result of myTasks.lenght-1");
console.log(myTasks.length);
console.log(myTasks);

cities.length--;
console.log(cities);

// We can't do the same on strings
let fullName = "Jamie Noble"
console.log(fullName.length);

fullName.lenght = fullName.length-1
console.log(fullName.length);

fullName.lenght--
console.log(fullName);

let theBeatles = ['John', 'Paul', 'Ringo', 'George'];
theBeatles.length++
console.log(theBeatles);


// [SECTION] Reading from Arrays

/*
SYNTAX: 
    arrayName[index];
*/
console.log("Result of grades[2]");
console.log(grades[2]);
console.log("Result of computerBrands[7]");
console.log(computerBrands[7]);

/*
let computerBrands = ['Acer', 'Asus', 'Lenovo', 'Neo', 'Redfox', 'Gateway', 'Toshiba', 'Fujitsu'];
*/
console.log("Result of computerBrands[8]");
console.log(computerBrands[8]);

let lakersLegends = ['Kobe', 'Shaq', 'Lebron', 'Magic', 'Kareem'];
console.log('Result of lakersLegends[0]');
console.log(lakersLegends[0]);
console.log('Result of lakersLegends[2]');
console.log(lakersLegends[2]);

let currentLaker = lakersLegends[2];
console.log("Result of currentLaker: ")
console.log(currentLaker);

console.log("Array before reassignment: ");
console.log(lakersLegends);

lakersLegends[2] = "Pau Gasol";
console.log("Array after reassignment: ");
console.log(lakersLegends);

// Accessing the last element of an array
let bullsLegends = ['Jordan', 'Pippen', 'Rodman', 'Rose', 'Kukoc'];

let lastElementIndex = bullsLegends.length - 1;
console.log(bullsLegends[lastElementIndex]);


console.log(bullsLegends[bullsLegends.length - 1]);

// Adding items into the Array

let newArr = [];
console.log(newArr[0]);

newArr[0] = 'Cloud Strife'; 
console.log(newArr);

newArr[1] = 'Tifa Lockhart'; 
console.log(newArr);

newArr[newArr.length] = "Barrett Wallace";
console.log(newArr);

// Looping over an Array

let numArr = [5, 12, 30, 46, 40];

for(let index = 0; index < numArr.length; index++ ) {
    if(numArr[index] % 5  === 0) {
        console.log(numArr[index] + " is divisible by 5");
    } else {
        console.log(numArr[index] + " is NOT divisible by 5")
    }
}

// [SECTION] Multidimensional array 
let chessBoard = [
    ['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
    ['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
    ['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
    ['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
    ['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
    ['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
    ['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
    ['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8']
];
console.table(chessBoard);
console.log(chessBoard);

console.log(chessBoard[2][5]);
console.log("Pawn moves to: " + chessBoard[3][6]);
console.log("Bishop moves to: " + chessBoard[3][1]);