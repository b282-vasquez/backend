/*
1. What directive is used by Node.js in loading the modules it needs?

Answer: require() function

2. What Node.js module contains a method for server creation?

Answer: "http"

3. What is the method of the http object responsible for creating a server using Node.js?

Answer: createServer() method

4. What method of the response object allows us to set status codes and content types?
	
Answer: response.setHeader("Content-Type", "text/html");
		response.setHeader("Status", "200");

Correct Answer: writeHead()

5. Where will console.log() output its contents when run in Node.js?

Answer: In the console or terminal window.

6. What property of the request object contains the address's endpoint?

Answer: `req.url` property provides the URL path portion of the incoming request, including the endpoint or route requested by the client.

*/