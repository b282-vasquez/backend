//Add solution here

let http = require("http"); 

const port = 3000;

const app = http.createServer((request, response) => {
	
	if(request.url == '/login') {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Welcome to login page.");
	} else {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("I'm sorry the page you are looking for cannot be found.");
	}
	
});

app.listen(port);

console.log(`Server running at localhost: ${port}`);


//Do not modify
module.exports = {app}