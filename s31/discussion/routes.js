//Create a server


let http = require("http"); 

const port = 3000;

const server = http.createServer((request, response) => {
	
	//"request.url" property in node.js that provide information about the URL of the incoming HTTP request.
	// "/greeting" - end point
	if(request.url == '/greeting') {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Hello world!");
	} else if (request.url == '/homepage') {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("This is the homepage!");
	} else {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end("Page not available");
	}
	
});

server.listen(port);

console.log(`Server running at localhost: ${port}`);