/*
Node.js Introduction
An introduction to Server-side programming

Server-side programming - process ng pagde develope at pag execute ng code.

*/

/*
Client-server architecture

> Develop a static fron-end application
> Create database 

dynamic content.
need to send request to a server that would in turn query and manipulate the database for us.

BENEFITS:
- Centralized data makes applications more scalable and maintainable.
- Multiple client apps may all use dynamically-generated data.
- Workload is concentrated on the server, making client apps lightweight.
- Improves data availability.
*/
/*For JS developers, we have Node.js for building server-side applications.*/

/*What is Node.js?
	- open-source, JS runtime environment for creating server-side applications.


	Runtime Environment
		- gives the context for running a programming language
		- JS was initally within the context of the browser, giving it access to:
			> DOM
			> window object
			> local storage
			> etc.

		- With Node.js the context was taken out of the browser and put into the server.
		- With Node.js, JS now has access to the following:
			> System resources
			> Memory
			> File System
			> Network
			> etc.
	
	BENEFITS:
		> Performance
			- Optimized for web applications.
		> Familiarity
			- "Same old" JavaScript(codes/syntax).
		> Access to Node Package Manager (NPM)
			- World's largest registry of packages.

*/

///////////////////////////////////////////////////////////////////////////////////////////
// Creating a Simple SERVER

let http = require("http");	
http.createServer(function (request, response) 
{

	response.writeHead(200, {'Content-Type' : 'text/plain'});
	response.end('Hello world!');

}).listen(4000);

console.log('Server running at localhost: 4000');

/*
1.) require() derectived 
	- used to LOAD NODE.JS MODULE
		** - MODULE
				- a SIMPLE or COMPLEX functionality ORGANIZED in SINGLE or MULTIPLE JavaScript FILES which can be REUSED throughout the Node.js application.

2.) "http" 
		- MODULE that lets the node.js TRANSFER DATA using the HTTP (Hyper Text Transfer Protocol).

3.) .createServer() method 
		- used to CREATE an HTTP SERVER that LISTEN to REQUEST on a specified port and GIVE RESPONSES back to the CLIENT.

4.) ("REQUEST/REQ", response)
		- messeges SENT by the CLIENT (usually a web browser).

5.) (request,"RESPONSE/RES")
		- messages SENT by the SERVER as an ANSWER.

6.) .writeHead() method
		- used to SET a STATUS CODE for RESPONSE and set the CONTENT TYPE of the response.
			**- Content Type sample :
					- {'Content-Type' : 'text/plain'}

7.) "200" 
		- A succesful request.

8.) .end() method
		- used to SEND the RESPONSE with text content 'Hello World'.

9.) .listen(portNumber) method
		- is node.js USED to START a SERVER LISTENING for INCOMING CONNECTIONS on a SPECIFIED PORT.

Example of PORT
		- "port = 4000"

Killing a Port
	SYNTAX:
		- npx kill-port portNumber
	Example :
		- npx kill-port 4000
*/ 
