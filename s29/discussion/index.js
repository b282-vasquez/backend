/*
MongoDB -Query Operators and Filed Projection
 	- Expand queries in MongoDB using Query Operators.


Query Operators
	- allow for more flexible querying of data within MongoDB
	 - Extract value that we needed.

NOTE: we must enclose the query operator as an object {}.

$gt and $lt Operator
	- Greater than and Less than

$regex Operator
	- look for a partial match within a given field.
*/

/*
Field Projection

Including or Exluding Fields
1 = Show
0 = Hide
*/
/////////////////////////////////////////////////
/*Comparison Query Operators*/
/*
$gt and $gte Operator
- Allows us to find documents that have field number values greater than or equal to a specified value.
	- Syntax
		db.collectionName.find({ field : { $gt : value } });
		db.collectionName.find({ field : { $gte : value } });

*/
db.users.find({age: {$gt : 50}});
db.users.find({age: {$gte : 50}});


/*
$lt and $lte Operator
	 - Allows us to find documents that have field number values less than or equal to a specified value.
Syntax
		db.collectionName.find({ field : { $lt : value } });
		db.collectionName.find({ field : { $lte : value } });*/
db.users.find({age: {$lt : 50}});
db.users.find({age: {$lte : 50}});

/////////////////////////////////////////////////////
/*
$ne Operator - (NOT EQUAL OPERATOR)
	- Allows us to find documents that have field number values not equal to a specified value.

SYNTAX:
	db.collectionName.find({field: {$ne : value}});
*/

db.users.find({age: {$ne: 82}});

/////////////////////////////////////////////////

/*
$in Operator (INCLUDED OPERATOR)
	- Allows us to find documents wit SPECIFIC MATCH CRITERIA on field using different values
SYNTAX: 
	db.collectionNae=me.find({field: {$in : value}});
*/

db.users.find({lastName: {$in: ["Hawking","Doe"]}});
db.users.find({courses: {$in: ["HTML","React"]}});

////////////////////////////////////////////////////

/*LOGICAL QUERY OPERATORS*/

/*
$or Operator (OR OPERATOR)
	- Allows us to find documents that match a single criteria from multiple provided search criteria.

SYNTAX: 
	- db.collectionName.find({$or: [{fieldA: valueA,fieldB: valueB}]});
*/
db.users.find({$or: [{firstName: "Neil"},{age: 21}]});

////////////////////////////////////////////////////

/*
$and Operator
	- Allows us to find documents matching multiple criteria in a single field.
SYNTAX:
	db.collectionName.find({$: [{fieldA: valueA},{fieldB: valueB}]});
*/

db.users.find({$and: [ {age:{$ne: 82}}, {age:{$ne:76}} ] });

////////////////////////////////////////////////////

/*FIELD PROFECTION*/
/*

Inclusion
	- Allows us to include/add specific fields only when retrieving documents.
	- the value provided is 1 to denote that the field is being included.

SYNTAX:
	 db.users.find({criteria},{field: 1});
*/

db.users.find (
		{firstName: "Jane"},
		{firstName: 1, lastName: 1, contact: 1}
	);

/*

Exclusion
	- Allows us to exlude/remove specific fields only when retrieving documents. 
	- The value provided is 0 to denote that the field is being exluded.

SYNTAX:
	 db.users.find({criteria},{field: 0});
*/

db.users.find (
		{firstName: "Jane"},
		{contact: 0, department: 0}
	);

////////////////////////////////////////////////////
/*
SUPRESSING the ID FIELD
	- Allows us to exclude the "_id" field when retrieving documents.
	- When we are using field projection, filed inclusion and exclusion may not be used at the same time.
	- Exluding the "_id" field is the only exception to this rule.
SYNTAX:
db.users.find({criteria},{_id:0});
*/
db.users.find (
		{firstName: "Jane"},
		{firstName: 1, lastName: 1, contact: 1, _id:0}
	);

/////////////////////////////////////////
/*Returning Specific Fields in Embedded Documents*/

db.users.find(
		{
			firstName: "Jane"
		},
		{
			firstName: 1,
			lastName: 1,
			"contact.phone": 1
		}

);

//////////////////////////////////////////
/*Project Specific Array Elements in the Returned Array*/
/*
$slice Operator
	- Allows us to retrieve elements that matches the search criteria.
*/

db.users.find(
	{
		"namearr" : 
		{
			namea: "juan"
		}
	},
	{
		namearr: {$slice: 1} //extract a portion of an Array.
	}

);

/*EVALUATION QUERY OPERATORS*/
/*
$regex Operator (REGULAR EXPRESSION OPERATOR)
	- Allows us to find documents that match a specific string patter using regular expressions.
SYNTAX: 
	db.users.find({field: $regex:'pattern', $options : 'optionValue'});
*/
//case sensitive query
db.users.find({firstName: {$regex: 'N'}});

//case insensitive query
db.users.find({firstName: {$regex: 'j', $options: 'i'} });