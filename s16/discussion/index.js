// console.log("Hello World!");

// [Section] Arithmetic Operators
// Arithmetic Operators allow matheatical operations

let x = 4;
let y = 12;

let sum = x+ y;
console.log ("Result of additon operator: " + sum); // Result: 16

let difference = x - y; // result: -8
console.log ("Result of subtraction operator: " + difference);

let product = x * y; // result: 48
console.log ("Result of multiplication operator: " + product);

let qoutient = x / y; // result: 0.3333333333333333
console.log ("Result of division operator: " + qoutient);

let remainder = x % y;
console.log ("Result of modulo operator: " + remainder);

// [SECTION] Assignment Operators
/*
Basic Assignment Operator (=)
    - The assignment operator assigns the value of the right operand to a variable.
*/

let assignmentNumber = 8;
console.log(assignmentNumber); // result: 8

/*Addition Assignment Operator (+=)
    - ADDS the VALUE of the right operand to a variable and ASSIGN the RESULT to the VARIABLE.
*/

// Example of LONG HAND
assignmentNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + assignmentNumber); 
// Result: 10

// Example of SHORT HAND
assignmentNumber += 2;
console.log("Result of addition assignment operator: " + assignmentNumber); 
// Result: 12
/*
Multiple Operators and Parenthesis

MDAS
M - Multiplication 3 * 4 = 12
D - Division 12 / 5 = 2.4
A - Addition 1 + 2 = 3
S - Subtraction 3 - 2.4 = 0.6
*/
let mdas = 1 + 2 -3 * 4 / 5;
console.log("Result of mdas operation " + mdas); // result: 0.6000000000000001


/*
PEMDAS
P - Parenthesis
E - Exponent
M - Multiplication 
D - Division 
A - Addition 
S - Subtraction

1.) 4 / 5 = 0.8
2.) 2 - 3 = -1
3.) -1 * 0.8 = -0.8
4.) 1 + -.08 = 0.19999999999999996
*/
let pemdas = 1 + (2 - 3) * (4 / 5);
console.log("Result of pemdas operation " + pemdas); // result: 0.19999999999999996


/*
Increment (++) and Decrement (--)
    - Operators that ADD or SUBTRACT values BY 1 and REASSIGNS the value of the variable WHERE the increment/decrement was APPLIED to.
*/

let z = 1;

let increment = z++;
console.log("Result of increment: " + z) //  result: 2

let decrement = z--;
console.log("Result of decrement: " + z) //  result: 1

// [SECTION] Type Coersion
/*
Type Coercion 
    - is the automatic or implicit CONVERSION of values FROM one data type to ANOTHER.
    - The values are AUTOMATICALLY CONVERTED from ONE DATA TYPE to ANOTHER in order to RESOLVE operations.
*/

let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion); // 1012
console.log(typeof coercion); // string

// [SECTION] COmparison Operators

/*
Equality Operator (==)
    - Checks whether the operands are equal/have the SAME CONTENT.
*/
console.log("Equality Operator")
console.log(1 == 1); // True
console.log(1 == '1'); // True
console.log('juan' == "juan"); // True
console.log(0 == false); // (0 = false) True

/*
Strict Equality Operator (===)
    - Checks whether the operands are equal/have the same content and also COMPARES the DATA TYPES of the two values.
*/
console.log("Strict Equality Operator")
console.log(1 === 1); // True
console.log(1 === '1'); // False
console.log('juan' === "juan"); // True 
console.log(0 === false); // (0 = false) False


 /*
 Inequality Operator (!=)
    - Checks whether the operands are NOT EQUAL or HAVE DIFFERENT content. 
 */
 console.log("inequality Operator")
 console.log(1 != 1); // false
 console.log(1 != '1'); // false
 console.log('juan' != "juan"); // false
 console.log(0 != false); // (0 = false) false

/* 
Strict Inequality Operator (!==)
    - Checks whether the operands are NOT EQUAL or HAVE DIFFERENT content and ALSO COMPARES that DATA TYPE of the two values.
*/
 console.log("Strict inequality Operator")
 console.log(1 !== 1); // False
 console.log(1 !== '1'); // True
 console.log('juan' !== "juan"); // False 
 console.log(0 !== false); // (0 = false) True

 // [SECTION] Relational Operations

 let a = 50;
 let b = 65;

 //  GT or Greater Than Operator ( > )
 let isGreaterThan = a > b; // false

 // LT or Less Than Operator ( < )
 let isLessThan = a < b; // true

 // GTE or Greater Than or Equal Operator ( >= )

 let isGTorGTE = a >= b; // False

 // LTE or Less Than or Equal Operator ( <= )

 let isLTorLTE = a <= b; // True

console.log("Relational operation")
console.log(isGreaterThan)
console.log(isLessThan)
console.log(isGTorGTE)
console.log(isLTorLTE)

// [SECTION] Logical Operators

let isLegalAge = true;
let isRegistered = false;

/*
Logical 'AND' Operator (&&)
    - Returns TRUE if ALL operands are TRUE.
*/
let allRequirmentsMet = isLegalAge && isRegistered;
console.log("Result of logical AND Operator: " + allRequirmentsMet); // result: False

/*
Logical 'OR' Operator ( || )
     - Returns TRUE if ONE of the operands is TRUE.
*/
let someRequirmentsMet = isLegalAge || isRegistered;
console.log("Result of logical AND Operator: " + someRequirmentsMet) // True

// Logical 'NOT' Operator (!)
// Returns OPPOSITE value
let someRequirementsNotMet = !isRegistered;
console.log("Result of logical NOT operator: " + someRequirementsNotMet);

