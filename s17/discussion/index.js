// console.log("Hello World!");

// [SECTION] Functions
/*
Functions 
	- are LINES or BLOCKS of CODES that tell our device or application to PERFORM a CERTAIN TASK when CALLED or INVOKED.
*/

/*
Function Declarations

SYNTAX:
	function functionName() {
		code block (statement)
	}
*/

/*
Functions 
	- are named to be able to be used later in the code
Function Block {} 
	- the statements which comprise the body of the function
*/

function //<--function - keyword used to define a JS functions
printName() //<--printName - an example of a 'function name'.
{
	console.log("My name is John"); //<--This is an example of code block (statement).
}

printName(); //<--Function invocation


// [SECTION] Function Declations vs Expressions
/*
Function Declarations
	- A FUNCTION can BE CREATED through 'FUNCTION DECLARATION' by using 'FUNCTION' keyword and ADDING a function NAME.
*/

declaredFunction();

function declaredFunction() //<--- This is an example of a 'FUNCTION DECLARATION'.
{
	console.log("Hello World from declaredFunction()!");
}
declaredFunction(); //<--- This is an example of a 'FUNCTION INVOCATION'.

/*
Function Expressions
	NOTE: A FUNCTION can also be STORED in a VARIABLES.

function expression
	- is an ANONYMOUS FUNCTION assigned to a 'variableFunction'

Anonymous function 
	- FUNCTION WITHOUT a name.
*/

// variableFunction(); // Uncaught ReferenceError: Cannot access 'variableFunction' before initialization

let variableFunction = function() {
	console.log("Hello again!");
}
variableFunction(); 

/*
Note: 
	- 'Function Expressions' are always INVOKED or CALLED using VARIABLE NAME.
*/
let funcExpression = function funcName() {
	console.log("Hello from the other side!");
}
// funcName(); // Uncaught ReferenceError: funcName is not defined
funcExpression();


/*
NOTE:
	- You can REASSIGN 'FUNCTIO DECLARATION' AND 'FUNCTION EXPRESSION' to NEW ANNONYMOUS functions.
*/
declaredFunction = function(){
	console.log("Updated declaredFunction!");
}
declaredFunction();

funcExpression = function() {
	console.log("Updated funcExpression!");
}
funcExpression();


const constantFunc = function() {
	console.log("Initialized with const!")
}
constantFunc();

/* NOTE: Cannot be reassigned

constantFunc = function() {
	console.log("Cannot be reassigned!")
}
constantFunc(); // Uncaught TypeError: Assignment to constant variable.
*/

// [SECTION] Function scoping
/*
Scope 
	- is the ACCESIBILITY or VISIBILITY of variables.

3 Types of Scope

1. local/block scope
2. global scope
3. function scope

*/

let globalVar = "Mr. Worldwide";
{
	let localVar = "Armando Perez";
}

console.log(globalVar);
// console.log(localVar); // Uncaught ReferenceError: localVar is not defined

function showNames() {

	// function scoped variables
	var functionVar = "Joe";
	const functionConst = "John";
	let functionLet = "Jane";

	console.log(functionVar);
	console.log(functionConst);
	console.log(functionLet);
}
showNames();
// console.log(functionVar); will result to error
// console.log(functionConst); will result to error
// console.log(functionLet); will result to error

// Global Scoped Variable
let globalName = "Alexandro"

function myNewFunction2() {
	let nameInside = "Renz";

	console.log(globalName);
}
myNewFunction2();


/*
Nested Functions
	- You can CREATE ANOTHER functon INSIDE a function.
*/
function myNewFunction() {
	let name = "B282";

	function nestedFunction(){
		let nestedName = "PT Class"
		console.log(name);
	}
	nestedFunction();
}
myNewFunction();

// [SECTION] Using alert()
/*
alert() 
	- allows us to show a SMALL WINDOW at the TOP of our BROWSER PAGE to SHOW INFORMATION to our users.
*/
// alert("Hello World!"); //<-- This will run immediately when the page loads

/* EXAMPLE:

function showSampleAlert(){
	alert("Hello, User!")
}
showSampleAlert();

console.log("I will only log in the console when the alert is dismissed.");
*/

// [SECTION] Using prompt()
/*
prompt() 
	- allows us to show a small window at the top of the browser to GATHER USER INPUT.
*/
let samplePrompt = prompt("Enter your name");
console.log("Hello, " + samplePrompt);

function printWelcomeMessage() {
	let firstName = prompt("Enter your first name.");
	let lastName = prompt("Enter your last name.");

	console.log("Hello, " + firstName + " " + lastName + "!");
	console.log("Welcome to my page!")
}
printWelcomeMessage();


// [SECTION] Function Naming Conventions

/*
	- Function names should be DEFINITIVE of the TASK it will PERFORM. 
	- It usually CONTAINS a VERB.
*/
function getCourses() {
	let courses = ["Science 101", "Math 101", "English 101"]
	console.log(courses);
}
getCourses();

/*
Name your functions in small caps
Follow 'camelCase' when naming variables
*/
function displayCarInfo(){
	console.log("Brand: Toyota");
	console.log("Type: Sedan");
	console.log("Price: 1,500,000");
}
displayCarInfo();

// AVOID generic names to avoid confusion within your code
function get() {
	let name = "Jamie";
	console.log(name);
}
get();


// Avoid pointless and inappropriate function name
function foo() {
	console.log(25%5);
}
foo();