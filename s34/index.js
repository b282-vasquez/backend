// Use the 'require' directive to load the express module/package.
const express = require("express");

//Create an application using express
//"app" is our server.
const app = express();

const port = 3000;

//m Method used from express.js are middleware
//Middleware - is sofware that provides services and capabilities to application outside of what's offered by the operating system.

// allows your app to read json data.
app.use(express.json()); //convert to JSON format
//allows your app to read data from any kinds of forms
app.use(express.urlencoded({extended: true}));

//[SECTION] Routes
// GET Method
app.get("/greet", (request, response) => {
	// "response.send" method to send a response back to the client.
	response.send("Hello from the /greet endpoint!")
})

// POST Method
app.post("/hello", (request, response) => {
	response.send(`Hello there, ${request.body.firstName} ${request.body.lastName}!`);
});

//Simple Registration_Start

let users = [];

app.post("/signup", (request,response) => {
	if(request.body.username !== ""  && request.body.password !== ""){
		users.push(request.body);

		response.send(`User ${request.body.username} successfully reqistered!`)
	} else {
		response.send(`Please input BOTH username and password!`)
	}
})

//Simple Registration_END

//[SECTION] Activity s34_START

app.get("/home", (request, response) => {
	response.send(`Welcome to the home page`)
})


app.get("/users", (request, response) => {
	response.send(users)
})

app.delete("/delete-user", (request, response) => {
    const username = request.body.username;
    let message;

    if (users.length > 0) {
        for (let i = 0; i < users.length; i++) {
            if(users[i].username === username) {
                users.splice(i, 1);
                message = `User ${username} has been deleted`;
                break;
            }
        }

    if(!message) {
        message = "User not exist.";
    }

    } else {
        message = "No users found.";
    }
    response.send(message);

});
//[SECTION] Activity s34_END




app.listen(port, () => console.log(`Server running at port ${port}`));

