/*
Aggregation in MongoDB and Query Case Studies.

	- An aggregate is a cluster of things that have come or have been brought togeher.

In mongoDB, aggregation operations group values from multiple documents together.

Aggregations are needed when your application needs a form of information that is not visibly available from your documents.

\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
Aggregation Pipelines
	- is a framework of data aggregation.

Stages of Aggreagation Pipemine 
	- Each statge transforms the docu as they pass through the pipeline. 

*/

/////////////////////////////////
// Create documents to use for the discussion
db.fruits.insertMany([
  {
    name : "Apple",
    color : "Red",
    stock : 20,
    price: 40,
    supplier_id : 1,
    onSale : true,
    origin: [ "Philippines", "US" ]
  },

  {
    name : "Banana",
    color : "Yellow",
    stock : 15,
    price: 20,
    supplier_id : 2,
    onSale : true,
    origin: [ "Philippines", "Ecuador" ]
  },

  {
    name : "Kiwi",
    color : "Green",
    stock : 25,
    price: 50,
    supplier_id : 1,
    onSale : true,
    origin: [ "US", "China" ]
  },

  {
    name : "Mango",
    color : "Yellow",
    stock : 10,
    price: 120,
    supplier_id : 2,
    onSale : false,
    origin: [ "Philippines", "India" ]
  }
]);


/*
Using the aggregrate method

SYNTAX:
db.collectionName.aggregate([
	{$match: {filed: value}},
	{$group: {_id: "field", total: {operation}}}
]);

/////////////////////////////////////

$match 
	- is used to pass documents that meet specified condition(s) to the next pipeline stage/agrreation process

$group
	- is used to group elements togheter and field-value pairs using the data from the grouped elements.

"$" symbol
	- refer to a field name that is available in the documents that are being aggregated on. 

*/

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);


/*
Field Projection with Aggregation
SYNTAX:
	{$project : {field: 1 or 0}}
*/

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$project : {_id: 0}}
]);


/*
Sorting aggregated result
SYNTAX:
	{$sort : {field: 1 or -1}}
*/

db.fruits.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}},
	{$sort : {total: 1}}
]);

/*Aggregating Results Based on Array Fields
SYNTAX:
	{$unwind: field}

$unwind 
	- deconstructs an array field from a collection with an array values to output a result for each element.
*/

db.fruits.aggregate([
	{$unwind: "$origin"}
]);

/*Display fruits documents by their origin and the kinds of fruits that are supplied.*/
db.fruits.aggregate([
	{$unwind: "$origin"},
	{$group: {_id: "$origin", kind_of_fruits: {$sum: 1}
	}}
]);
