//Node.js Routing w/ HTTP Methods
//Access URL endpoints using HTTP methods
/*
HTTP METHODS
Commonly used HTTP methods
GET - Retrieve resources.
POST - Send data for creating a resource.
PUT - Send data for updating a resource.
DELETE - Deletes a pecified resource.

>> We will now Use Postman Client

*/



let http = require("http");

http.createServer(function(request, response)
	{

		// method: GET
		if(request.url == "/items" && request.method == "GET") {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end(`Data retrieved from the database`);
		}

		// method: POST
		if(request.url == "/items" && request.method == "POST") {
		response.writeHead(200,{'Content-Type' : 'text/plain'});
		response.end(`Data to be sent to the database`);
		}

	}).listen(4000);

	console.log(`Server is running at localhost: 4000`);