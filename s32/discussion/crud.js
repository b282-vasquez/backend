let http = require("http");

// creating mock database start
let directory = [
	{
		"name" : "Paeng",
		"email" : "mr.vasquezr@gmail.com"
	},
	{
		"name" : "Ira Reyes",
		"email" : "mrs.vasquezr@gmail.com"
	}
]
//mock database end


http.createServer(function(request, response)
	{
		//GET method
		if(request.url == "/users" && request.method == "GET") {

			// Sets response output to JSON data type.
			response.writeHead(200, {'Content-Type': 'application/json'})
			// write() - method in node.js that is used to write data to the response body in HTTP server.
			// JSON.stringify() method - converts the string input to JSON.
			response.write(JSON.stringify(directory));
			response.end()
		};	//END of curly brace of Line 20
			
		// POST METHOD
		if(request.url == "/users" && request.method == "POST"){
			
			let requestBody = ''; // in POST MAN: body>raw>JSON
				
				request.on('data', function(data){
					requestBody += data;
				});
			 
				request.on('end', function(){
			    requestBody = JSON.parse(requestBody);
			    
			    let newUser = {
			    	"name" : requestBody.name,
			    	"email" : requestBody.email
			    }

			    directory.push(newUser)
			    console.log(directory);

				response.writeHead(200,{'Content-Type': 'application/json'});
				response.write(JSON.stringify(newUser));
				response.end();
				});
			} //END of curly brace of Line 30


	}).listen(3000);

	console.log(`Server is running at localhost: 3000`);