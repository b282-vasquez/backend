let http = require("http");

const app = http.createServer(function (request, response) {

    //Add code here
    if(request.url == "/" && request.method == "GET") {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end(`Welcome to booking system`);
		}
	if(request.url == "/profile" && request.method == "GET") {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end(`Welcome to your profile`);
		}
	if(request.url == "/courses" && request.method == "GET") {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end(`Here's our courses available`);
		}	
	if(request.url == "/addCourses" && request.method == "POST") {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end(`Add courses to our resources`);
		}
	if(request.url == "/updateCourses" && request.method == "PUT") {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end(`Update a courses to our resources`);
		}
	if(request.url == "/archiveCourses" && request.method == "DELETE") {
		response.writeHead(200, {'Content-Type' : 'text/plain'});
		response.end(`Archieve courses to our resources`);
		}
})


















//Do not modify
//Make sure to save the server in variable called app
if(require.main === module){
    app.listen(4000, () => console.log(`Server running at port 4000`));
}

module.exports = app;