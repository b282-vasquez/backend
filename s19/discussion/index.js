// console.log("Hello World!");

// [SECTION] if, else if, and else Statement

// if Statement
// executes a statement if a specified condition is true

let numA = -1;

if(numA < 0) { //True
	console.log("Hello!");
}

if(numA > 0) { //False
	console.log("Hello!");
}


// else if Clause
// executes a statement if previous conditions are false and if the specified condition is true.

let numB = 1;

if(numA > 0) { //False
	console.log("Hello!");
	} else if(numB > 0) { //True
		console.log("World!");
	}

// else Statement 
// Executes a statement if all other conditions are false

if(numA > 0) { //False
	console.log("Hello!");
	} else if(numB == 0) { //False
		console.log("World!");
	} else {
		console.log("Hello World!");
	}


/*
windSpeed < 30 → Not a typhoon yet.
windSpeed <= 61 → Tropical depression detected.
windSpeed >= 62 && windSpeed <= 88 → Tropical storm detected.
windSpeed >= 89 && windSpeed <= 117 → Severe tropical storm detected
*/

// if, else if, and else statement with functions

let message;

function determineTyphoonIntensity(windSpeed) {

	if(windSpeed < 30) {
		// return statement is used within a function to specify the value that should be returned when the function is called.
		return 'Not a typhoon'
	}
		else if(windSpeed <= 61) {
			return 'Tropical depression detected.'
		}

		else if(windSpeed >= 62 && windSpeed <= 88){
			return 'Tropical storm detected.'
		}

		else if(windSpeed >= 89 && windSpeed <= 117){
			return 'Severe tropical storm detected'
		}

		else {
			return 'Typhoon detected'
		}

}
message = determineTyphoonIntensity(65);
console.warn(message);

if(message == 'Tropical depression detected.') {
	// console.warn() - is a good way to print warning in our console that could help us developer act on certain output within our code.
	console.warn(message);
}


// [SECTION] Truthy and Falsy 
// In JS a "truthy" value is a value that is considered true when encountered in a Boolean context.
// Values are considered true unless defined otherwise

/*
Falsy values/ exeption for truthy:
1. false
2. 0
3. -0
4. ""
5. null
6. undefined
7. NaN
*/

// Truthy samples 
if(true) {
	console.log('Truthy');
}
if(1) {
	console.log('Truthy');
}
if([]) {
	console.log('Truthy');
}

// Falsy examples
if(false) {
	console.log ('Falsey');
}

if(0) {
	console.log ('Falsey')
}

if(undefined) {
	console.log ('Falsey');
}


// [SECTION] Conditional (Ternary) Operator
// Can be used as an alternative to an "if else" statement
// Ternary operators have an implicit "return" statement.

/*
The conditional (ternary) operator takes in 3 operands:
1. condition
2. expression to execute if the condition is truthy.
2. expression to execute if the condition is falsy.
*/

/*
SYNTAX
	(condition) ? ifTrue : ifFalse;
*/
// Single statement execution
let ternaryResult = (1 < 18) ? true : false;
console.log("Result of Ternary Operator: " + ternaryResult);

// Multiple statement execution

let name;

function isOfLegalAge() {
	name = "John";
	return 'You are of the legal age limit'
} 
function isUnderAge() {
	name = "Jane";
	return 'You are under the legal age limit'
}

// The "parseInt()" function converts the input received into numebr datatype.
/*
let age = parseInt(prompt("What is your age?"));
console.log(age);

let legalAge = (age >= 18) ? isOfLegalAge() : isUnderAge();
console.log("Result of Ternary Operator in function: " + legalAge + ', ' + name);
*/

// [SECTION] Switch Statement
// Can be used as an alternative to an if, else if, and else statement where the data to be used in the condition is of an expected input.

/*
SYNTAX: 
	switch (expression) {
		case value: 
		statement;
		break
	default:
		statement;
		break;
	}
*/
// ".toLowerCase()" - function/method will change the input receive from the prompt into all lowercase letters.
let day = prompt("What day of the week is it today?").toLowerCase();
console.log(day);

// switch cases are considered as "loops", meaning it will compare the expression wuth each of the case values until a match is found.
switch (day) {
	case 'monday' : 
		console.log("The color of the day is red");
		//"break;" - used to terminate the current loop once a match has been found.
		break;
	case 'tuesday' :
		console.log("The color of the day is orange");
		break;
	case 'wednesday':
	    console.log("The color of the day is yellow");
	    break;
	case 'thursday':
	    console.log("The color of the day is green");
	    break;
	case 'friday':
	    console.log("The color of the day is blue");
	    break;
	case 'saturday':
	    console.log("The color of the day is indigo");
	    break;
	case 'sunday':
	    console.log("The color of the day is violet");
	    break;

	default:
		console.log("PLease input a valid day!");
		break;
}

// [SELECTION] Try-Catch-Finally Statement 
// "try catch" statements are commonly used for error handling.

function showIntensityAlert(windSpeed) {

	try {
		// Attemt to execute a code
		alerat(determineTyphoonIntensity(windSpeed));
	} catch (error){
		// "typeof" operator is used to check the data type of a value/expression and returns a string value of what the data type is.
		console.log(typeof error);
		//"error.message" - is used to access the information relating to an error object.
		console.warn(error.message)
	} finally {
		// continue execution of code regardless of success and failure of code execution in the "try" block to hadle/resolve errors.
		alert("Intensity update will show new alert")
	}

}
showIntensityAlert(56);