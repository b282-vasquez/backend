// console.log("Hello World");

/*
	JSON
		- Stands for JavaScript Object Notation
		- Used also in OTHER programming languages.
		- serializing different data type into bytes
		- cold strings
		- JSON HELPS us STORE and TRANSMIT data conviniently.

NOTE: Core JavaScript HAS a BUILT-IN 'JSON object' that contains methods for 'parsing' JSON Objects and CONVERTING STRINGS into JavaScripts Objects.

	SYNTAX:
		{
			"propertyA" : "valueA",
			"propertyA" : "valueA"
		};

NOTE:
	JSON
		- are WRAPPED in CURLY BRACES.

	Properties and Value
		- are WRAPPED in DOUBLE QUOTES.

*/

/*EXAMPLE*/
console.log("EXAMPLE of JavaScript Object Notion ( JSON )");
	let sample1 = `
		{
			"name": "Mochi",
			"age":20,
			"address":{
				"city":"Tokyo",
				"country":"Japan"
			}
		}
	`;
	console.log(sample1);
	/*
	MINI ACTIVITY 1:

	1. Create a variable that will hold a "JSON" and create a person object.

	2. Log the variable and send a SS.*/

	let person = `
		{
			"name":"Paeng",
			"age":29,
			"address":{
				"city" : "Imus",
				"country" : "Philippines"
			}
		}
	`;
	console.log("SOLUTION of MINI Activity 1 ");
	console.log(person);
	
	console.log("Data Type: " + typeof person); // string

	/*Question:
	Are we ABLE to TURN a JSON into a JS Object?
	Answer: YES

	JSON.parse()
		- will RETURN the JSON as an 'JS OBJECT'.
	*/

	console.log("Turning JSON(string data type) as an JS Object(object data type):");
	console.log(JSON.parse(sample1));
	console.log(JSON.parse(person));

	/*
	JSON ARRAY
		- is an ARRAY of JSON
	*/
	console.log("EXAMPLE of JSON ARRAY");
	let sampleArr = `
		[
			{
				"email":"zenitsu@proton.me",
				"password":"agatsuma",
				"isAdmin":true
			},

			{
				"email":"mochi@gmail.com",
				"password":"mochimochi",
				"isAdmin":false
			},

			{
				"email":"json@gmail.com",
				"password":"friday1",
				"isAdmin":false
			}
		]
	`;
	console.log(sampleArr);
	
	console.log("Data Type: " + typeof sampleArr);

	/* QUESTION
	1. Can we use ARRAY METHOD on a JSON Array? (sampleArr) 
	Answer : NO, Because a JSON is a STRING.

	2. What can we do to be ABLE to ADD more ITEMS or OBJECTS into our sampleArr?
	Answer: 'PARSE' the JSON array for us to USE Array Methods and SAVED it in the variable.
		(JSON.parse() - will RETURN the JSON as an 'JS OBJECT')
	*/
	console.log("EXAMPLE of Converting JSON Array to JS Object using JASON.parse(sampleArr)");
	let parsedSampleArr = JSON.parse(sampleArr);
	console.log(parsedSampleArr);
	console.log("Data Type: " + typeof parsedSampleArr);

	/*Question:
	1. Can we now DELETE the LAST ITEM in the JSON Array?
	Answer: Yes.

	2. What method should we use?
	Answer: pop() method.
	*/

	console.log("Deleting the Last Item of converted JSON Array using '.pop()' method"); 
	console.log(parsedSampleArr.pop()); 
	console.log(parsedSampleArr);

	/* NOTE:
	If for example we need to send this data back to our client/ Front End, it should be in JSON Format.


	JSON.stringify
		- this will stringify JS object as JSON.
		JS Object ---> JSON using .stringify

	NOTE: JSON.parse()
			- does not mutate or update the original JS

	Therefore, we can actually TURN a JS object INTO a JSON.
	*/

	sampleArr = JSON.stringify(parsedSampleArr);
	console.log("JS Object ---> JSON using '.stringify'")
	console.log(sampleArr);

	/* Backend
	Database (JSON) => Server/API (JSON to JS Object to process) => sent as JSON (frontend/client).
	*/
	/*Mini Activity
		1. Given the JSON array, process it and convert to a JS so we can manipulate the array

		2. Delete the last item in the array and add a new item in the array

		3. Stringify the array back in JSON

		4. Update the jsonArr with the stringified array

		5. Log the jsonArr and send an SS.
	*/

	/*Example for Mini Activity*/
	let jsonArr = `
		[
			"pizza",
			"Hamburger",
			"spaghettii",
			"hotdog",
			"pancit bihon"
		]
	`
// Activity Solution:
	// 1.
	let parsedJsonArr = JSON.parse(jsonArr)
	// 2.
	console.log(parsedJsonArr);
	console.log(parsedJsonArr.pop()); 
	console.log(parsedJsonArr.push("Graham Cake")); 
	// 3. and 4.
	jsonArr = JSON.stringify(parsedJsonArr);
	// 5.
	console.log(jsonArr);


// Gather User Details
let firstname = prompt("What is your first name?");
let lastname = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("From which city do you live in?"),
	country: prompt("From which country does your city address belong to??")
};

let otherData = JSON.stringify({
	firstname: firstname,
	lastname: lastname,
	age: age,
	address: address
})
console.log("Gather User Details");
console.log(otherData);

let sample3 = `
{
	"name": "Cardo",
	"age": 18,
	"address": {
		"city":"Quiapo",
		"country":"Philippines"
	}
}

`;
try{
	console.log(JSON.parse(sample3))
}
catch(err){
	console.log("This will result to an error. Use double quotes for properties and string values.")
	console.log(err)
	}finally{
		console.log("This will run!")
	}

