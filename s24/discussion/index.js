// console.log("Hello World!");

// [SECTION] Exponent Operator

console.log("Exponent Operator:");
/*
Old version: 

	Math.pow
Math.pow - method takes two arguments, the base and the exponent.
*/
//Old way Start
const firstNum = Math.pow(8, 2); //64
console.log(firstNum);
//Old way End

/*
New version:

	(**) operator
		- is used for exponentation
*/
//New way Start
const secondNum = 8 ** 2; //64
console.log(secondNum);
//New way End

// [SECTION] Template Literals
/*
	- Allows to write strings without using concatenation operator "+"

	- backticks(``) and ${}

*/
// Old version start
let name = "Rafael";
let message = 'Hello ' + name + '! Welcome to programming!'
console.log(message);
//old version end

// New version start
message = `Hello ${name}! Welcome to programming!`;
console.log(message);
// New version End



// Mini Activity Start

const interestRate = .1;
const principal = 1000;
console.log(`The interest on your savings accont is: ${interestRate * principal}`); //The interest on your savings accont is: 100

// Mini Activity End



// [SECTION] Array Destructuring
/* - Allows unpacked elements in arrays into distinct.

SYNTAX:
	let/const [variableName, variableName,variableName..] = arrayName 
	*/

//Old way Start
const fullName = ['Juan','Dela','Cruz'];
console.log(fullName[1]); //Dela
//Old way End

//New way Start
const [firstName, middleName, lastName] = fullName;
console.log(middleName); //Dela
//New way End


// [SECTION] Object Destructuring
/* - Allow unpacked elements in objects into distinct.

SYNTAX:
	let/const {propertyName, propertyName, propertyName..} = objectName */

//Old way Start
const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
}
console.log(person.familyName); // Cruz
//Old way End

//New way Start
const { givenName, maidenName, familyName } = person;
console.log(familyName); // Cruz
//New way End


// [SECTION] Arrow Functions
/* - Allow us to write shorter function syntax.
	SYNTAX:
		let/const variableName = () => { 
		statement
		}
*/


const students = ["John", "Jane", "Judy"];

//traditional way Start
students.forEach(function(student) {
	console.log(`${student} is a student.`)
});
//Old way End

//New way Start
console.log("Result from using Arrow function: ")
students.forEach((student) => {
	console.log(`${student} is a student.`)
})
//New way End


// [SECTION] Implicit Return Statement
/* - THere are instances when you can omit/removed the "return" statement.
*/

// Old way Start
/*
const add = function(x, y) {
	return x + y;
}
const total = add(1, 2);
console.log(total); // 3
*/
// Old way End

// New way Start
console.log("Result from using Arrow function: ");

const add = (x,y) => x + y;

const total = add(1, 2); //invoked
console.log(total); // 3*/
// New way End


// [SECTION] Default Function Argument Value
/* - PRovides a default argument value if non is provided when the function is invoked. 
	example: (name = "user")
*/
const greet = (name = "User") => {
	return `Good morning, ${name}!`;
}
console.log(greet());
console.log(greet("PAENG"));

// [SECTION] Class-Based Object Blueprints
/* - Allows creation/instatiation of object using classes as blueprints.
*/

// New way Start
class Car {
	constructor(brand, name, year) {
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}

const myCar = new Car();
console.log(myCar);

// Values of properties may be assigned after creation/instatiation of objects. 
myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;
console.log(myCar);

// Creating/Instantiating a new object from "car class" with initialized values
const myNewCar = new Car ("Toyota", "Vios", 2021);
console.log(myNewCar);
// New way End

