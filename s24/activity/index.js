// console.log("Hello World");
//Important Note: Do not change the variable names. 
//All required classes, variables and function names are listed in the exports.

// Exponent Operator
const getCube = 2 ** 3;

// Template Literals
let message = `The cube of 2 is ${getCube}`;
console.log(message);

// Array Destructuring
const address = ["258", "Washington Ave NW", "California", "90011"];
const[houseNumber, street, state, zipCode] = address;

let myAddress = `I live at ${houseNumber} ${street}, ${state} ${zipCode}.`
console.log(myAddress);

// Object Destructuring
const animal = {
	name: "Lolong",
	species: "saltwater crocodile",
	weight: "1075 kgs",
	measurement: "20 ft 3 in"
}

const {name, species, weight, measurement} = animal

let animalInfo = `${name} was a ${species}. He weighed at ${weight} with a measurement of ${measurement}.`;
console.log(animalInfo);

// Arrow Functions
let numbers = [1, 2, 3, 4, 5];

numbers.forEach((number) => 
	console.log(number)
);


const reduceNumber = numbers.reduce(
  (accumulator, currentValue) => accumulator + currentValue
);

console.log(reduceNumber);


// Javascript Classes

class Dog {
	constructor(name, age, breed,){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}

const dogInfo = new Dog();

dogInfo.name = "Frankie"
dogInfo.age = 5
dogInfo.breed = "Miniature Dachshund"

console.log(dogInfo);

//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        getCube: typeof getCube !== 'undefined' ? getCube : null,
        houseNumber: typeof houseNumber !== 'undefined' ? houseNumber : null,
        street: typeof street !== 'undefined' ? street : null,
        state: typeof state !== 'undefined' ? state : null,
        zipCode: typeof zipCode !== 'undefined' ? zipCode : null,
        name: typeof name !== 'undefined' ? name : null,
        species: typeof species !== 'undefined' ? species : null,
        weight: typeof weight !== 'undefined' ? weight : null,
        measurement: typeof measurement !== 'undefined' ? measurement : null,
        reduceNumber: typeof reduceNumber !== 'undefined' ? reduceNumber : null,
        Dog: typeof Dog !== 'undefined' ? Dog : null

    }
} catch(err){

}