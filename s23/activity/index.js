// console.log("Hello World");

//Strictly Follow the property names and spelling given in the google slide instructions.
//Note: Do not change any variable and function names. 
//All variables and functions to be checked are listed in the exports.

// Create an object called trainer using object literals

let trainer = {
	name: 'Paeng',
	age: 10,
	pokemon: ['Pikachu', 'Muk', 'Diglet', 'Pigeon'],
	friends: {
		town1: ['Misty','Rock'],
		town2: ['Max','Doc']
			},
	talk:function() {
		return "Pikachu! I choose you!"
		}
};
console.log(trainer);
console.log("Trainer's Name: ");
console.log(trainer.name);
console.log("Trainer's Pokemons");
console.log(trainer['pokemon']);
console.log(trainer.talk())


function Pokemon(name, level) {
  // Properties
  this.name = name;
  this.level = level;
  this.health = level * 2; 
  this.attack = level * 1; 

  // Method: Tackle
  this.tackle = function(target) {
          console.log(this.name + ' tackled ' + target.name);
          console.log(target.name + 's health is now reduced to' + Number(target.health -= this.attack));
          if (target.health <= 0) {
              target.faint();
          }
      };
      this.faint = function() {
          console.log(this.name + ' fainted.');
    };
}


let myPokemon = new Pokemon("Pikachu", 10)
console.log(myPokemon);

myPokemon = new Pokemon('Geodude', 15)
console.log(myPokemon);

myPokemon = new Pokemon('Mewtwo', 150)
console.log(myPokemon);


myPokemon = new Pokemon('Pikachu', 10);
let targetPokemon = new	Pokemon('Geodude', 1);

myPokemon.tackle(targetPokemon);

// Creating Pokémon objects
let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo", 100);

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);

geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);




//Do not modify
//For exporting to test.js
//Note: Do not change any variable and function names. All variables and functions to be checked are listed in the exports.
try{
    module.exports = {

        trainer: typeof trainer !== 'undefined' ? trainer : null,
        Pokemon: typeof Pokemon !== 'undefined' ? Pokemon : null

    }
} catch(err){

}